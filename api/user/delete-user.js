'use strict';

const AWS = require('aws-sdk');

AWS.config.update({region: 'us-east-1'});
const dynamoDB = new AWS.DynamoDB.DocumentClient();

module.exports.handler = async event => {

    const userID = event.requestContext.authorizer.userID;

    const deleteParams = {
        TableName: 'userTable',
        Key: {
            "userID": userID
        }
    }

    try {
        //Delete user and return success
        await dynamoDB.delete(deleteParams).promise();
        return formatResponse(200, JSON.stringify({message: "User deleted"}));
        
    } catch (err) {
        return formatResponse(400, JSON.stringify({error: err}));
    }
}

//Format the response to an HTTP method
const formatResponse = (statusCode, body) => {
    return {
        statusCode,
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': true
        },
        body
    };
}