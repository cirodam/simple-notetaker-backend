'use strict';

const AWS = require('aws-sdk');

AWS.config.update({region: 'us-east-1'});
const dynamoDB = new AWS.DynamoDB.DocumentClient();

module.exports.handler = async event => {

    const userID = decodeURIComponent(event.pathParameters.userID);

    //Get the user and return if they don't exist
    const user = await getUser(userID);
    if(!user){
        return formatResponse(404, JSON.stringify({error: "User not found"}));
    }

    //Return everything but the password
    const {firstName, lastName, email} = user;
    return formatResponse(200, JSON.stringify({userID, firstName, lastName, email}));
}

//Format the response to an HTTP method
const formatResponse = (statusCode, body) => {
    return {
        statusCode,
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': true
        },
        body
    };
}

//Get a user from the user table given the specified userID
const getUser = async userID => {
    const params = {
        TableName: 'userTable',
        KeyConditionExpression: '#n = :n',
        ExpressionAttributeValues: {
            ':n' : userID
        },
        ExpressionAttributeNames: {
            '#n' : 'userID'
        }
    }

    try {
        const res = await dynamoDB.query(params).promise();
        if(res.Count > 0){
            return res.Items[0];
        }
        return null;
    } catch (err) {
        return err;
    }
}