'use strict';

const AWS = require('aws-sdk');
const uuid = require('uuid');
const bcrypt = require('bcryptjs');

AWS.config.update({region: 'us-east-1'});
const dynamoDB = new AWS.DynamoDB.DocumentClient();

module.exports.handler = async event => {

    const request = JSON.parse(event.body);
    const {firstName, lastName, email, password} = request;

    //Prevent duplicate emails
    if(await checkForEmail(email) === true){
        return formatResponse(400, JSON.stringify({error: "A user already exists with that email"}));
    }

    //Encrypt password and generate user
    const encryptedPass = await encrpytPassword(password);
    const userID = uuid.v4();

    //Add user and return if there is a problem
    if(await addUser(userID, firstName, lastName, email, encryptedPass) === false){
        return formatResponse(400, JSON.stringify({error: "Error adding user"}));
    }

    //Return new user
    return formatResponse(200, JSON.stringify({userID, firstName, lastName, email}));
};

//Check if there is a user with the specified email
const checkForEmail = async email => {

    const params = {
        TableName: 'userTable',
        IndexName: 'emailIndex',
        KeyConditionExpression: '#e = :e',
        ExpressionAttributeValues: {
            ':e' : email
        },
        ExpressionAttributeNames: {
            '#e' : 'email'
        }
    }

    try {
        const res = await dynamoDB.query(params).promise();

        if(res.Count > 0){
            return true;
        }
        return false;

    } catch (err) {
        return err;
    }
}

//Encrypt the provided password and return
const encrpytPassword = async password => {
    const salt = await bcrypt.genSalt(10);
    return await bcrypt.hash(password, salt);
}

//Format the response to an HTTP method
const formatResponse = (statusCode, body) => {
    return {
        statusCode,
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': true
        },
        body
    };
}

//Add a user to the user table
const addUser = async (userID, firstName, lastName, email, password) => {

    const params = {
        TableName: 'userTable',
        Item: {
            userID,
            firstName,
            lastName,
            email,
            password
        }
    }

    try {
        await dynamoDB.put(params).promise();
        return true;

    } catch (err) {
        return false;
    }
}