'use strict';

const AWS = require('aws-sdk');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('config');

AWS.config.update({region: 'us-east-1'});
const dynamoDB = new AWS.DynamoDB.DocumentClient();

module.exports.handler = async event => {

    const {email, password} = JSON.parse(event.body);

    //Get user from the email
    const user = await getUserFromEmail(email);
    if(!user){
        return formatResponse(400, JSON.stringify({error: "Incorrect Email or password"}));
    }

    //Check if the provided password matches this user's password
    const isMatch = await bcrypt.compare(password, user.password);
    if(!isMatch){
        return formatResponse(400, JSON.stringify({error: "Incorrect email or password"}));
    }

    //Generate jwt and return
    const {userID, firstName, lastName} = user;
    const payload = {userID};
    const token = jwt.sign(payload, config.get("JWT_SECRET"), {expiresIn: 120});
    let expires = Date.now() + 120*1000;
    return formatResponse(200,  JSON.stringify({token, expires, user:{userID, firstName, lastName, email}}));
}

//Format the response to an HTTP method
const formatResponse = (statusCode, body) => {
    return {
        statusCode,
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': true
        },
        body
    };
}

//Query the users table to find the user with the specified email
const getUserFromEmail = async email => {

    const params = {
        TableName: 'userTable',
        IndexName: 'emailIndex',
        KeyConditionExpression: '#e = :e',
        ExpressionAttributeValues: {
            ':e' : email
        },
        ExpressionAttributeNames: {
            '#e' : 'email'
        }
    }

    try {
        const res = await dynamoDB.query(params).promise();
        if(res.Count > 0){
            return res.Items[0];
        }
        return null;

    } catch (err) {
        return null;
    }
}
