'use strict';

const AWS = require('aws-sdk');

AWS.config.update({region: 'us-east-1'});
const dynamoDB = new AWS.DynamoDB.DocumentClient();

module.exports.handler = async event => {

    const userID = event.requestContext.authorizer.userID;
    const noteID = decodeURIComponent(event.pathParameters.noteID);

    const params = {
        TableName: 'noteTable',
        KeyConditionExpression: '#n = :n',
        ExpressionAttributeValues: {
            ':n' : noteID
        },
        ExpressionAttributeNames: {
            '#n' : 'noteID'
        }
    }

    try {
        //Get the specified note, return if it doesn't exist
        const res = await dynamoDB.query(params).promise();
        if(res.Count === 0){
            return formatResponse(404, JSON.stringify({error: "Note not found"}));
        }

        //Check if the note belongs to the user that requested it
        const note = res.Items[0];
        if(note.noteOwner !== userID){
            return formatResponse(401, JSON.stringify({error: "Unauthorized"}));
        }

        //Return the note
        return formatResponse(200, JSON.stringify(note));

    } catch (err) {
        return formatResponse(400, JSON.stringify({error: err}));
    }
}

//Format the response to an HTTP method
const formatResponse = (statusCode, body) => {
    return {
        statusCode,
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': true
        },
        body
    };
}