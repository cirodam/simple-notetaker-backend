'use strict';

const AWS = require('aws-sdk');

AWS.config.update({region: 'us-east-1'});
const dynamoDB = new AWS.DynamoDB.DocumentClient();

module.exports.handler = async event => {

    const userID = event.requestContext.authorizer.userID;
    const noteID = decodeURIComponent(event.pathParameters.noteID);

    //Get the specified note
    const note = await getNote(noteID);
    if(!note){
        return formatResponse(404, JSON.stringify({error: "Note not found"}));
    }

    //Check if this user is authorized to delete it
    if(note.noteOwner !== userID){
        return formatResponse(401, JSON.stringify({error: "Unauthorized"}));
    }

    try {
        //Delete note and return success
        await deleteNote(noteID);
        return formatResponse(200, JSON.stringify({noteID}));

    } catch (err) {
        return formatResponse(400, JSON.stringify({error: err}));
    }
}

//Format the response to an HTTP method
const formatResponse = (statusCode, body) => {
    return {
        statusCode,
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': true
        },
        body
    };
}

//Get a note from the note table given the specified noteID
const getNote = async noteID => {
    const params = {
        TableName: 'noteTable',
        KeyConditionExpression: '#n = :n',
        ExpressionAttributeValues: {
            ':n' : noteID
        },
        ExpressionAttributeNames: {
            '#n' : 'noteID'
        }
    }

    try {
        const res = await dynamoDB.query(params).promise();
        if(res.Count > 0){
            return res.Items[0];
        }
        return null;

    } catch (err) {
        return null;
    }
}

//Delete the note from the note table with the specified noteID
const deleteNote = async noteID => {

    const params = {
        TableName: 'noteTable',
        Key: {
            "noteID": noteID
        }
    }

    try {
        await dynamoDB.delete(params).promise();
        return {
            statusCode: 200,
            body: JSON.stringify({message: "Note deleted"})
        };
        
    } catch (err) {
        return {
            statusCode: 400,
            body: JSON.stringify({error: err})
        };

    }
}