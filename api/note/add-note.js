'use strict';

const AWS = require('aws-sdk');
const uuid = require('uuid');

AWS.config.update({region: 'us-east-1'});
const dynamoDB = new AWS.DynamoDB.DocumentClient();

module.exports.handler = async event => {

    const userID = event.requestContext.authorizer.userID;
    const request = JSON.parse(event.body);
    
    const params = {
        TableName: 'noteTable',
        Item: {
            noteID: uuid.v4(),
            noteOwner: userID,
            noteTitle: request.noteTitle,
            noteText: request.noteText,
            noteCreation: Date.now()
        }
    }

    try {
        //Add new note and return success
        await dynamoDB.put(params).promise();
        return formatResponse(200, JSON.stringify(params.Item));

    } catch (err) {
        return formatResponse(400, JSON.stringify({error: err}));
    }
};

//Format the response to an HTTP method
const formatResponse = (statusCode, body) => {
    return {
        statusCode,
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': true
        },
        body
    };
}
