'use strict';

const AWS = require('aws-sdk');

AWS.config.update({region: 'us-east-1'});
const dynamoDB = new AWS.DynamoDB.DocumentClient();

module.exports.handler = async event => {

    const userID = event.requestContext.authorizer.userID;
    const noteID = decodeURIComponent(event.pathParameters.noteID);
    const request = JSON.parse(event.body);

    //Get note and return if it doesn't exist
    const note = await getNote(noteID);
    if(!note){
        return formatResponse(404, JSON.stringify({error: "Note not found"}));
    }

    //Check note owner
    if(note.noteOwner !== userID){
        return formatResponse(401, JSON.stringify({error: "Unauthorized"}));
    }

    const params = {
        TableName: 'noteTable',
        Key: {
            noteID 
        },
        UpdateExpression: 'set noteCreation = :t',
        ExpressionAttributeValues: {
            ':t' : Date.now()
        },
        ReturnValues: 'ALL_NEW'
    }

    if(request.noteTitle) {
        params.UpdateExpression += ', noteTitle = :r';
        params.ExpressionAttributeValues[':r'] = request.noteTitle;
    }

    if(request.noteText) {
        params.UpdateExpression += ', noteText = :s';
        params.ExpressionAttributeValues[':s'] = request.noteText;
    }

    try {
        //Update note and return success
        const res = await dynamoDB.update(params).promise();
        return formatResponse(200, JSON.stringify(res.Attributes));

    } catch (err) {
        return formatResponse(400, JSON.stringify({error: err}));
    }
}

//Format the response to an HTTP method
const formatResponse = (statusCode, body) => {
    return {
        statusCode,
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': true
        },
        body
    };
}

//Get a note from the note table given the specified noteID
const getNote = async noteID => {
    const params = {
        TableName: 'noteTable',
        KeyConditionExpression: '#n = :n',
        ExpressionAttributeValues: {
            ':n' : noteID
        },
        ExpressionAttributeNames: {
            '#n' : 'noteID'
        }
    }

    try {
        const res = await dynamoDB.query(params).promise();
        if(res.Count > 0){
            return res.Items[0];
        }
        return null;

    } catch (err) {
        return null;
    }
}