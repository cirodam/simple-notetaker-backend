'use strict';

const AWS = require('aws-sdk');

AWS.config.update({region: 'us-east-1'});
const dynamoDB = new AWS.DynamoDB.DocumentClient();

module.exports.handler = async event => {

    const userID = event.requestContext.authorizer.userID;

    const params = {
        TableName: 'noteTable',
        IndexName: 'ownerIndex',
        KeyConditionExpression: '#o = :o',
        ExpressionAttributeValues: {
            ':o' : userID
        },
        ExpressionAttributeNames: {
            '#o' : 'noteOwner'
        }
    }

    try {
        //Retrieve notes and return them
        const res = await dynamoDB.query(params).promise();
        return formatResponse(200, JSON.stringify(res.Items));

    } catch (err) {
        return formatResponse(400, JSON.stringify({error: err}));
    }
}

//Format the response to an HTTP method
const formatResponse = (statusCode, body) => {
    return {
        statusCode,
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': true
        },
        body
    };
}